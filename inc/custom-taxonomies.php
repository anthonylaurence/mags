<?php


function register_mags_taxonomy() {
	$labels = array(
		'name'					=> __('Mags Issues'),
		'menu_name'				=> __('Mags Issues'),
		'singular_name'			=> __('Mags Issue'),
		'all_items'				=> __('All Mags Issues'),
		'edit_item'				=> __('Edit Mags Issue'),
		'view_item'				=> __('View Mags Issue'),
		'update_item'			=> __('Update Mags Issue'),
		'add_new_item'			=> __('Add New Mags Issue'),
		'new_item_name'			=> __('New Mags Issue'),
		'search_items'			=> __('Search Mags Issues'),
		'popular_items'			=> __('Popular Mags Issues'),
		'add_or_remove_items'	=> __('Add or Remove Mags Issues'),
		'choose_from_most_used'	=> __('Select from popular Mags Issues'),
		'not_found'				=> __('No Mags Issues Found'),
	);

	$rewrite = array(
		'slug'					=> 'mags',
		'with_front'			=> false,
		'hierarchical'			=> false,
		'ep_mask'				=> 'EP_NONE' ,
	);

	$capabilities = array(
		'manage_terms'			=> 'manage_categories',
		'edit_terms'			=> 'manage_categories',
		'delete_terms'			=> 'manage_categories',
		'assign_terms'			=> 'edit_posts',
	);

	$args = array(
        'label'					=> __('Mags'),
		'labels'				=> $labels,
		'public'				=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> false,
		'show_in_nav_menus'		=> true,
		'show_tagcloud'			=> false,
		'show_in_quick_edit'	=> true,
		'meta_box_callback'		=> true,
		'show_admin_column'		=> true,
		'description'			=> 'Taxonomy added to posts to make them available for the Mags plugin.',
		'hierarchical'			=> true,
		'update_count_callback'	=> null,
		'rewrite'				=> true,
		'capabilities'			=> $capabilities,
		'sort'					=> false,
	);
	register_taxonomy('mags', array('register_mags_article_list'), $args);
}



?>