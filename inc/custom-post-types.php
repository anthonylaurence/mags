<?php
function register_mags_article_list() {
	$labels = array(
		'name'					=> __('Mags TOC'),
		'menu_name'				=> __('Mags TOC'),
		'singular_name'			=> __('Mags TOC page'),
		'add_new'				=> __('Add New Mags TOC page'),
		'add_new_item'			=> __('Add New Mags TOC page'),
		'edit_item'				=> __('Edit Mags TOC page'),
		'new_item'				=> __('New Mags TOC page'),
		'view_item'				=> __('View Mags TOC page'),
		'search_items'			=> __('Search Mags TOC page'),
		'not_found'				=> __('No Mags TOC page Found'),
		'not_found_in_trash'	=> __('No Mags TOC page Found in Trash')
		);
	$args = array(
		'label'					=> __('Mags TOC'),
		'labels'				=> $labels,
		'description'			=> 'Article List post types for our Mags publications.',
		'exclude_from_search'	=> true,
		'publicly_queryable'	=> true,
		'show_in_nav_menus'		=> true,
		'show_ui'				=> true,
        'show_in_menu'			=> true,
        'taxonomies'            => array('mags'),
		'hierarchical'			=> true,
		'supports'				=> array('title', 'editor', 'thumbnail', 'excerpt')
		);
	register_post_type('mags_article_list', $args);
}

function register_mags_article() {
	$labels = array(
		'name'					=> __('Mags Article'),
		'menu_name'				=> __('Mags Article'),
		'singular_name'			=> __('Mags Article page'),
		'add_new'				=> __('Add New Mags Article page'),
		'add_new_item'			=> __('Add New Mags Article page'),
		'edit_item'				=> __('Edit Mags Article page'),
		'new_item'				=> __('New Mags Article page'),
		'view_item'				=> __('View Mags Article page'),
		'search_items'			=> __('Search Mags Article page'),
		'not_found'				=> __('No Mags Article page Found'),
		'not_found_in_trash'	=> __('No Mags Article page Found in Trash')
		);
	$args = array(
		'label'					=> __('Mags Article'),
		'labels'				=> $labels,
		'description'			=> 'Article post types for our Mags publications.',
		'exclude_from_search'	=> true,
		'publicly_queryable'	=> true,
		'show_in_nav_menus'		=> true,
		'show_ui'				=> true,
        'show_in_menu'			=> true,
        'taxonomies'            => array('mags'),
		'hierarchical'			=> true,
		'supports'				=> array('title', 'editor', 'thumbnail', 'excerpt')
		);
	register_post_type('mags_article', $args);
}




?>