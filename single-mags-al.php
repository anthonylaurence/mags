<?php
get_header();

while ( have_posts() ) {
	the_post();
	?>
	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
		  <div class="row small-uncollapse medium-uncollapse">
			<div class="small-12 medium-8 large-8 columns" id="maincol">
		  			<div class="entry-content">

		  <header>
			    	<h1 class="headline"><?php the_title(); ?>: Table of Contents</h1>
			    	<?php echo '<div class="row">';
				 echo '<div class="small-12 columns">';
				 echo '	<div class="magazine-nav">';
				 echo 	wp_nav_menu( array(
										'menu'   => 'bp-magazine',
										'container' => false,
										'depth' => 0,
										'items_wrap' => '<ul class="inline-list">%3$s</ul>',
										'fallback_cb' => 'haven_menu_fallback',
								) );
				echo '</div>';
				echo '</div>';
				echo '</div>'; ?>
			    </header>
		</div>

			 	<?php
            	// check if the post has a Post Thumbnail assigned to it.
					if ( has_post_thumbnail() ) { ?>
						<div class="single-thumbnail-wrapper aligncenter">
							<?php the_post_thumbnail('magazine-thumbnail');?>
						</div>

					<?php }
					if (is_user_logged_in()) {
					if ( $bluetoad = get_field('bluetoad_issue_url') ) {
					if ( $bluetoad ) { ?>
						<div class="aligncenter" style="padding-top: 2%;">
						<?php
							echo '<a href="'.$bluetoad.'" target="_blank">View Full Print Magazine Edition</a>';
						 ?>
						</div>
					<?php	}
					}
					}
					?>

			</div>
			<div class="small-12 medium-4 large-4 columns rr">
				<div class="toc">
			    	<?php
			    	the_content();

			    	?>
			    </div>
			</div>
		</div>
	</article>
	<?php
}
haven_post_nav();

get_footer();
?>
