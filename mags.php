<?php
/**
 * Plugin Name: Mags
 * Plugin URI: http://www.mags.com
 * Description: Use your WordPress site to create an online magazine!!
 * Version: 0.1
 * Author: Anthony Laurence
 */

/**
 *	Changelog:
 *		0.1 - 08/26/2016 - Anthony Laurence
 *			Initial Beta
 *		0.2 - 2/17/2021 - Anthony Laurence
 *			- mods to template-mags-issues and css to get title, summary and FI to show via dropdown change
 *			- randomize admin nonce
 */

 /*
@TODO 
- get mag articles to show under Mags
- get AJAX working from admin area - no updates to functions.php
- drag and drop to order articles on Mags Issues page

 */


/*
 * Start the magic
*/

class Mags {

	// NEED TO ADD FLUSHING OF REWRITE RULES ON ACTIVATION

	public function activate() {
			add_option('mags', false);
		}
	public function mags_scripts() {
		//JS
			
		wp_enqueue_script( 'mags_ajax', plugins_url('/js/mags-ajax.js', __FILE__), array('jquery'));
		wp_enqueue_script( 'mags_loading', plugins_url('/js/loading.js', __FILE__));
		wp_enqueue_script( 'jquery-ui-widget' );
		wp_enqueue_script( 'jquery-ui-mouse' );
		wp_enqueue_script( 'jquery-ui-accordion' );
		wp_enqueue_script( 'jquery-ui-autocomplete' );
		wp_enqueue_script( 'jquery-ui-slider' );

		//CSS
		wp_enqueue_style( 'mags_loading_jq_plugin_styles', plugins_url('/css/loading-style.css', __FILE__));
		wp_enqueue_style( 'mags_admin_issues_styles', plugins_url('/css/mags-styles.css', __FILE__));
		$wp_scripts = wp_scripts();
		wp_enqueue_style(
		'jquery-ui-theme-smoothness',
		sprintf(
			'//ajax.googleapis.com/ajax/libs/jqueryui/%s/themes/smoothness/jquery-ui.css', // working for https as well now
			$wp_scripts->registered['jquery-ui-core']->ver
		)
		);

			
	}
	


    public function __construct() {
		register_activation_hook(__FILE__, array($this,'activate'));
		
		//set up custom taxonimies
		require_once(plugin_dir_path(__FILE__) . "/inc/custom-taxonomies.php");

		//set up custom post types
		require_once(plugin_dir_path(__FILE__) . "/inc/custom-post-types.php");

		add_action('init',  'register_mags_article_list');
		add_action('init', 'register_mags_taxonomy');
		add_action( 'admin_enqueue_scripts', array($this,'mags_scripts') );
		add_action('admin_menu', array($this, 'admin_menu'));
		add_action( 'wp_ajax_remove_taxonomy', array($this, 'remove_taxonomy_from_post') );
		add_action( 'wp_ajax_get_article_list', array($this, 'mags_get_article_list') );
		add_action( 'add_meta_boxes', array($this, 'mags_register_issue_meta_boxes') );
		add_action( 'save_post', array($this, 'mags_save_issue_metabox'));

		add_action('init', array($this, 'ajax_init') );  



	}

	public function ajax_init(){
		//AJAX calls
		add_action( 'wp_ajax_new_issue', array($this,'ajax_create_new_mags_issue') );
	}

	public function admin_menu() {
			add_menu_page (__('Mags Issues'), __('Mags Issues'), 'manage_options', 'mags', array($this,'mags_admin_page'), 'dashicons-megaphone');
			//add_submenu_page('mags', 'Mags Articles', 'Mags Articles', 'manage_options', 'mags-articles', );
	
	}

	public function mags_admin_page() {
		//template for list of all Mag Issues
		require_once( plugin_dir_path(__FILE__) . "templates/template-mags-issues.php");
	}
		public function remove_taxonomy_from_post(){

			//handle removing the taxonomy from the post via ajax
			global $wpdb;
			$taxonomy = 'mags';
			$e = wp_remove_object_terms( $_POST['id'], $_POST['tax'], $taxonomy );
			return $e;
			if(is_wp_error($e) ) {
				return "There was an error removing the article. Please try again.";
			} else {
				return "Article removed successfully.";
			}

			wp_die();
		}

		public function mags_get_article_list(){
			$args = array(
				'post_type' => 'mags_article_list'
			);

			$posts = json_encode(get_posts($args) );

			wp_send_json($posts);
		}

		public function mags_register_issue_meta_boxes() {
			add_meta_box( 'mags-issues-taxonomy', __( 'Mag Issue', 'textdomain' ), array($this,'mags_issue_metabox'), 'post' );
		}

		public function mags_issue_metabox(){
			global $post;
			$mags = $this->get_all_mags();
			$this_post_terms = get_the_terms($post->ID, 'mags');
			;

			echo '<select name="mags_issue"><option value="0">Select Mag Issue....</option>';
			
				foreach($mags as $m){
					$selected = 0;
					if($this_post_terms !== false or !is_wp_error($this_post_terms) ){
						foreach($this_post_terms as $tpt){
							$selected = ( $tpt->term_taxonomy_id == $m->term_taxonomy_id)? 'selected': '';
						}
					}
					echo '<option value="'.$m->term_taxonomy_id.'" '.$selected.'>'.$m->name.'</option>';
				}
			
			echo "</select>";

			wp_nonce_field( 'mags_save_post_meta','mags_issue_metabox_nonce'); 

		}

		public function mags_save_issue_metabox(){
			if ( isset( $_POST['mags_issue_metabox_nonce'] ) || wp_verify_nonce( $_POST['mags_issue_metabox_nonce'], 'mags_save_post_meta' ) 
				) {
				// process form data
				global $post;
					if( $_POST['mags_issue'] > 0 && !empty($_POST['mags_issue']) ){
						wp_set_post_terms( $post->ID, $_POST['mags_issue'], 'mags' );

					} else if ( $_POST['mags_issue'] == 0 ){
						wp_delete_object_term_relationships( $_POST['ID'], 'mags' );
					} else {
						"Error - could not attach issue to post at this time.";
						return;
					}
				}
		}


		/***
		 * Helper Functions
		 */

		 public function get_all_mags(){
			$args = array(
				'taxonomy' => 'mags',
				'hide_empty' => false,
				'orderby' => 'name',
				'order' => 'ASC'
			); 
			return get_terms($args);
		 }



		 /**
		  * AJAX CALLS
		  */

		  public function ajax_create_new_mags_issue(){
			if( wp_verify_nonce( $_REQUEST['nc'], "mags_issues" ) ){

/*ADD CODE TO TAKE TITLE AND MAKE A MAGS TERM */

			}
		  }
}

$mags = new Mags();


?>