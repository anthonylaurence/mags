<?php

/*
Template for the List of all Mags Issues

*/

		global $wp_version;
		$ajax_nonce = wp_create_nonce(  'mags_issues' );
		?>
		
		<section id="mags">
			<header id="mags-header">
				<h1 class="mags-inline">Mags</h1>
				<diV>
					<button class="new_issue">Create New Issue</button>
				</div>
				<div id="new_issue">
					<h3>Issue Title: </h3><input type="text" name="new_issue_title" />
					<button class="mag-action save-mag mag-button" data-buttonaction="save_new">Save</button>
				</div>
				<div style="width: 100%;" class="mags-inline"></div>
			</header>
			<div class="message"></div>
			<input type="hidden" name="issues_nonce" value="<?php echo $ajax_nonce; ?>" />
		<?php 
		$m = new Mags();
		$mags = $m->get_all_mags();
		

		echo '<section id="mags-block"> ';
		//show all of our Mags (child taxonomies in the parent mags taxonomy)
		foreach($mags as $m){
			echo '<section class="mags-issue">';
			echo '<h1 class="mags-edition" data-slug="' . $m->slug . '">' . $m->name . '</h1>';

			//now get all of the TOC and create a JSON with the title, summary and image
			$mags_toc =	get_posts(array(
				'post_type' => 'mags_article_list',
				)
			);

		//	print_r($mags_toc);
			echo '<script>';
			echo 'var tocJSON = {
				0: {title: "",
					summary: "",
					image: ""},	
				';
				$i = 0;
				foreach($mags_toc as $toc){
					$summary =  get_the_excerpt($toc->ID); 
					$img = get_the_post_thumbnail_url( $toc,  'post-thumbnail' );
					echo ' '.$toc->ID.' : { ';
					echo 'title: "'.$toc->post_title.'", ';
					echo 'image: "'.$img.'",';
					echo 'summary: "'.$summary.'"}';
					if($i < count($mags_toc)-1 ){
						echo ',';
					}
					$i++;
				}
			echo '} </script>';

				//now show all of the posts associated with this Mags (child taxonomy)
				$mags_articles =	get_posts(array(
								'post_type' => 'post',
								'tax_query' => array(
									array(
										'taxonomy' => 'mags',
										'field' => 'slug',
										'terms' => $m->slug)
									)
								)
							);

							//echo "<pre>";print_r($sp_posts);exit;
				echo '<section class="mags-posts" id="' . $m->slug . '">';
				?>
				</ul>
				<form name='issue-form-<?php echo $m->slug;?>' class="mags-issue-form">
						<h3>Table of Contents: </h3>
						<p>ADD DROPDOWN OF MAGS TOC TO  CHOSE FROM - ONCE CHOSEN SHOW TITLE, SUMMARY AND FEATURED IMAGE</p>
						<select name="toc-<?php echo $m->slug;?>"  data-issue='<?php echo $m->slug;?>' class="toc-dropdown"> 
							<option value="0">Select TOC to apply to Issue...</option>
							<?php

								foreach($mags_toc as $toc){
									echo '<option value = "'.$toc->ID.'">'.$toc->post_title.'</option>';
								}

							?>
							</select>
							<section class="flex-container">
							<div>
								<h3 class="title">Title: <br>
									<span></span>
								</h3>
								<h3 class="summary">Summary: <br>
									<span></span>
								</h3>
							</div>
							<div>
								<h3 class="thumbnail">Featured Image: <br>
									<img />
								</h3>
							</div>
							</section>
				</form>
		<?php 
		echo '<h2>Articles List: </h2>
		<p><strong>Drag and Drop article to change their order. Click the X to remove the article from the list.</strong> </p>
		<ul>';
		foreach($mags_articles as $a){
			?>
			<li>
				<h2><a href="/wp-admin/post.php?post=<?php echo $a->ID ?>&action=edit" target="_blank"><?php echo $a->post_title ?></a></h2> <button >X</button>
				<input type="hidden" name="mags_order" value="<?php echo $a->ID ?>" />
			</li>
		<?php } 

		echo '<div>
		<button class="mag-action save-mag mag-button" data-id-slug="$m->slug" data-buttonAction="save">Save '.$m->name.'</button>
	</div>
	</section>	
	</section>			
	</section>';
}// end foreach loop

        
        ?>

<script>

	(function($) {
		var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
		$(document).ready(function(){
			$('.toc-dropdown').on('change', function(){
				var cur_toc = $(this).val();
				var issue = $(this).data('issue')
				//console.dir( $('form[name="issue-form-' + issue + '"]').find('.title') );
				$('form[name="issue-form-' + issue + '"]').find('.title').children('span').text(tocJSON[cur_toc].title);
				$('form[name="issue-form-' + issue + '"]').find('.summary').children('span').text(tocJSON[cur_toc].summary);
				$('form[name="issue-form-' + issue + '"]').find('.thumbnail').children('img').attr('src', tocJSON[cur_toc].image);
			});
		});
	})(jQuery);
		

</script>